QT += core
QT -= gui

TARGET = SystemCheck
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    systemcheck.cpp

HEADERS += \
    systemcheck.h

