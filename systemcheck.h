#ifndef SYSTEMCHECK_H
#define SYSTEMCHECK_H

#include <QObject>

#include <QHash>
#include <QTimer>
#include <QStringList>

class SystemCheck : public QObject
{
	Q_OBJECT
public:
	explicit SystemCheck(QObject *parent = 0);

	struct ProcessStat{
		int pid;
		QString user;
		int vsz;
		QString stat;
		QString command;
	};

	int getProcessStat();
	void addApps(const QString &app);
	void addErrorStats(const QString &stat);
	void start();
signals:

public slots:
protected:
	QList<ProcessStat> apps;
	void parseOutProcessStat(const QString &output);
	QTimer *timer;
	void printProcessStat();
	QString getAppStat(const QString &app);
	bool debug;
	QStringList checkList;
	QStringList errorStatList;
	QString getUptime();
	int countKillVk;
protected slots:
	void timeout();
};

#endif // SYSTEMCHECK_H
