#include <QCoreApplication>

#include <QDebug>
#include <systemcheck.h>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);
	if (a.arguments().last() == "--version") {
		qDebug() << "version: ship_v2";
		return 0;
	}

	SystemCheck *syscheck = new SystemCheck();
	syscheck->addApps("dm365_ipstr");
	syscheck->addApps("vksystem");
	syscheck->addErrorStats("Z");
	syscheck->addErrorStats("SW");
	syscheck->addErrorStats("DW");
	syscheck->start();
	return a.exec();
}

