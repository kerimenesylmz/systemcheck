#include "systemcheck.h"

#include <QProcess>
#include <QDebug>

SystemCheck::SystemCheck(QObject *parent)
	: QObject(parent)
{
	countKillVk = 0;
	debug = false;
}

void SystemCheck::start()
{
	timer = new QTimer();
	connect(timer, SIGNAL(timeout()), SLOT(timeout()));
	timer->setInterval(60 * 1000);
	timer->start();
	qDebug() << "watching out this apps;" <<
				checkList <<
				"with this stats;" <<
				errorStatList <<
				"in every 60*1000 miliseconds";
}

void SystemCheck::addApps(const QString &app)
{
	checkList << app;
}

void SystemCheck::addErrorStats(const QString &stat)
{
	errorStatList << stat;
}

int SystemCheck::getProcessStat()
{
	QProcess a;
	a.start("ps");
	if (!a.waitForFinished(3000)) {
		qDebug() << "Command didn't complete";
		return -1;
	}
	QByteArray ba = a.readAll();
	if (ba.isEmpty())
		return -2;
	parseOutProcessStat(QString::fromLatin1(ba.data()));
	return 0;
}

void SystemCheck::parseOutProcessStat(const QString &output)
{
	if (apps.size() > 0)
		apps.clear();
	QStringList lines = output.split("\n", QString::SkipEmptyParts);
	foreach (QString line, lines) {
		if (line.contains("PID"))
			continue;
		QStringList flds = line.split(" ", QString::SkipEmptyParts);
		if (flds.size() < 5)
			continue;
		ProcessStat proc;
		proc.pid = flds[0].toInt();
		proc.user = flds[1];
		proc.vsz = flds[2].toInt();
		proc.stat = flds[3];
		proc.command = flds[4];
		apps << proc;
	}
	if (debug)
		printProcessStat();
}

void SystemCheck::printProcessStat()
{
	if (apps.isEmpty())
		return;
	foreach (ProcessStat proc, apps) {
		qDebug() << proc.pid << proc.user << proc.vsz << proc.stat << proc.command;
	}
}

QString SystemCheck::getAppStat(const QString &app)
{
	if (apps.isEmpty())
		return "";
	foreach (ProcessStat proc, apps) {
		if (proc.command.contains(app))
			return proc.stat;
	}
	return "";
}

QString SystemCheck::getUptime()
{
	QProcess uptime;
	uptime.start("uptime");
	if (!uptime.waitForFinished(3000)) {
		qDebug() << "Command didn't complete";
		return "";
	}
	QString ba = uptime.readAll();
	QStringList flds = ba.split("\n").first().split(" ");
	flds.removeAll("");
	QString upStat = flds.at(3);
	QString untiltime = flds.at(2);
	if (upStat.contains("days"))
		return untiltime;
	return "";
}

void SystemCheck::timeout()
{
	getProcessStat();
	if (checkList.size() < 1) {
		qDebug() << "Application control list is empty";
		return;
	}
	foreach (QString app, checkList) {
		QString stat = getAppStat(app);
		if (errorStatList.contains(stat)) {
			qDebug() << QString("This application(%1) is going to hell!!! with this stat %2").arg(app).arg(stat);
			QProcess::execute("reboot");
		}
	}

	QString untiltime = getUptime();
	if (untiltime.isEmpty())
		return;
	if (untiltime.toInt() > (20 * (countKillVk +1))) {
		qDebug() << untiltime << countKillVk << (20 * (countKillVk +1));
		QProcess::execute("killall vksystem");
		countKillVk++;
	}
}
